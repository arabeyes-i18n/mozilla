#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-10-03 21:38+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Generator: Translate Toolkit 0.8\n"

#: en/whatsnew/index.html:1
msgid "<head>"
msgstr ""

#: en/whatsnew/index.html:2
msgid "Mozilla | Firefox Updated"
msgstr ""

#: en/whatsnew/index.html:3
msgid "You've Updated To The Latest Version of Firefox"
msgstr ""

#: en/whatsnew/index.html:4
msgid "For details on what's included in this update, please see the <a
href=\"http://en-US.www.mozilla.com/en-US/firefox/2.0/releasenotes/\">release notes</a>."
msgstr ""

#: en/whatsnew/index.html:5
msgid "Did you know about..."
msgstr ""

#: en/whatsnew/index.html:6
msgid "<a href=\"http://en-US.add-ons.mozilla.com/en-US/firefox/\">Firefox Add-ons</a>"
msgstr ""

#: en/whatsnew/index.html:7
msgid "You can choose from hundreds of free add-ons to extend the functionality of Firefox to add new features and personalize the overall look and feel of the browser to suit your own personal style."
msgstr ""

#: en/whatsnew/index.html:8
msgid "Add [Locale Name] Dictionary for Inline Spell Checking"
msgstr ""

#: en/whatsnew/index.html:9
msgid "<a href=\"https://en-US.add-ons.mozilla.com/en-US/firefox/2.0/dictionaries/\">Install dictionary now</a>"
msgstr ""

#: en/whatsnew/index.html:10
msgid "<a href=\"javascript:window.home();\">Go to my home page</a>"
msgstr ""

