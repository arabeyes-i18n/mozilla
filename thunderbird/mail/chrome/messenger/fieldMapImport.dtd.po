# translation of fieldMapImport.dtd.po to Arabic
# Khaled Hosny <khaledhosny@eglug.org>, 2007.
# extracted from thunderbird/mail/chrome/messenger/fieldMapImport.dtd
msgid ""
msgstr ""
"Project-Id-Version: fieldMapImport.dtd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-09-13 21:46+0300\n"
"PO-Revision-Date: 2007-09-06 08:59+0300\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Accelerator-Marker: &\n"

# ***** BEGIN LICENSE BLOCK *****
# Version: MPL 1.1/GPL 2.0/LGPL 2.1
# The contents of this file are subject to the Mozilla Public License Version
# 1.1 (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
# http://www.mozilla.org/MPL/#
# Software distributed under the License is distributed on an "AS IS" basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
# for the specific language governing rights and limitations under the
# License.#
# The Original Code is Mozilla Communicator client code, released
# March 31, 1998.#
# The Initial Developer of the Original Code is
# Netscape Communications Corporation.
# Portions created by the Initial Developer are Copyright (C) 1998-1999
# the Initial Developer. All Rights Reserved.#
# Contributor(s):#
# Alternatively, the contents of this file may be used under the terms of
# either the GNU General Public License Version 2 or later (the "GPL"), or
# the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
# in which case the provisions of the GPL or the LGPL are applicable instead
# of those above. If you wish to allow use of your version of this file only
# under the terms of either the GPL or the LGPL, and not to allow others to
# use your version of this file under the terms of the MPL, indicate your
# decision by deleting the provisions above and replace them with the notice
# and other provisions required by the GPL or the LGPL. If you do not delete
# the provisions above, a recipient may use your version of this file under
# the terms of any one of the MPL, the GPL or the LGPL.#
# ***** END LICENSE BLOCK *****
#: fieldMapImport.title
msgid "Import Address Book"
msgstr "استورد دفتر العناوين"

#: fieldMapImport.size
msgid ""
"_: Do not translate this.  Only change the numeric values if you need this dialogue box to appear bigger.\n"
"width: 40em; height: 30em;"
msgstr "width: 40em; height: 30em;"

#: fieldMapImport.recordNumber
msgid "Imported data for Record: "
msgstr "بينات السجل المستوردة:"

#: fieldMapImport.next
msgid "Next"
msgstr "التّالي"

#: fieldMapImport.next.accesskey
msgid "N"
msgstr "N"

#: fieldMapImport.previous
msgid "Previous"
msgstr "السابق"

#: fieldMapImport.previous.accesskey
msgid "P"
msgstr "P"

#: fieldMapImport.text
msgid "Use Move Up and Move Down to match the address book fields on the left to the correct data for import on the right. Uncheck items you do not want to import."
msgstr "استخدم انقل لأعلى ولأسفل لتقابل حقول دفتر العناوين على اليسار بالبيانات الصحيحة للاستيراد على اليمين. ارفع العلامة من العناصر التي لا تريد استيرادها."

#: fieldMapImport.up
msgid "Move Up"
msgstr "انقل لأعلى"

#: fieldMapImport.up.accesskey
msgid "U"
msgstr "U"

#: fieldMapImport.down
msgid "Move Down"
msgstr "انقل لأسفل"

#: fieldMapImport.down.accesskey
msgid "D"
msgstr "D"

#: fieldMapImport.fieldListTitle
msgid "Address Book fields"
msgstr "حقول دفتر العناوين"

#: fieldMapImport.dataTitle
msgid "Record data to import"
msgstr "بيانات السجل التي ستستورد"

#: fieldMapImport.skipFirstRecord
msgid "First record contains field names"
msgstr "السجل الأول يحتوي أسماء الحقول"

#: fieldMapImport.skipFirstRecord.accessKey
msgid "F"
msgstr "F"

