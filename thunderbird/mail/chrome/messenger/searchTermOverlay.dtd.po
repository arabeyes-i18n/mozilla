# translation of searchTermOverlay.dtd.po to Arabic
# Khaled Hosny <khaledhosny@eglug.org>, 2007.
# extracted from thunderbird/mail/chrome/messenger/searchTermOverlay.dtd
msgid ""
msgstr ""
"Project-Id-Version: searchTermOverlay.dtd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-09-13 21:46+0300\n"
"PO-Revision-Date: 2007-09-11 23:21+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\nnplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>= 1 && n%100<=99 ? 4 : 5;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Accelerator-Marker: &\n"

#: matchAll.label
#: matchAll.accesskey
msgid "M&atch all of the following"
msgstr "&طابق كلّ التالي"

#: matchAny.label
#: matchAny.accesskey
msgid "Match any &of the following"
msgstr "طا&بق أيًّا من التالي"

#: matchAllMsgs.label
#: matchAllMsgs.accesskey
msgid "&Match all messages"
msgstr "طاب&ق كل الرسائل"

# LOCALIZATION NOTE
# The values below are used to control the widths of the search widgets.
# Change the values only when the localized strings in the popup menus
# are truncated in the widgets.
#: searchTermListAttributesFlexValue
#: searchTermListOperatorsFlexValue
#: searchTermListValueFlexValue
msgid "5"
msgstr "5"

#: searchTermListButtonsFlexValue
msgid "2"
msgstr "2"

