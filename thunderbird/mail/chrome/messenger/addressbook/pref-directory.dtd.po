# translation of pref-directory.dtd.po to Arabic
# Khaled Hosny <khaledhosny@eglug.org>, 2007.
# extracted from thunderbird/mail/chrome/messenger/addressbook/pref-directory.dtd
msgid ""
msgstr ""
"Project-Id-Version: pref-directory.dtd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-09-13 21:46+0300\n"
"PO-Revision-Date: 2007-08-30 23:22+0300\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Accelerator-Marker: &\n"

# ***** BEGIN LICENSE BLOCK *****
# Version: MPL 1.1/GPL 2.0/LGPL 2.1
# The contents of this file are subject to the Mozilla Public License Version
# 1.1 (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
# http://www.mozilla.org/MPL/#
# Software distributed under the License is distributed on an "AS IS" basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
# for the specific language governing rights and limitations under the
# License.#
# The Original Code is Mozilla Communicator client code, released
# March 31, 1998.#
# The Initial Developer of the Original Code is
# Netscape Communications Corporation.
# Portions created by the Initial Developer are Copyright (C) 1998-1999
# the Initial Developer. All Rights Reserved.#
# Contributor(s):#
# Alternatively, the contents of this file may be used under the terms of
# either the GNU General Public License Version 2 or later (the "GPL"), or
# the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
# in which case the provisions of the GPL or the LGPL are applicable instead
# of those above. If you wish to allow use of your version of this file only
# under the terms of either the GPL or the LGPL, and not to allow others to
# use your version of this file under the terms of the MPL, indicate your
# decision by deleting the provisions above and replace them with the notice
# and other provisions required by the GPL or the LGPL. If you do not delete
# the provisions above, a recipient may use your version of this file under
# the terms of any one of the MPL, the GPL or the LGPL.#
# ***** END LICENSE BLOCK *****
# LOCALIZATION NOTE (window.title) : do not translate "LDAP" in below line
#: pref.ldap.window.title
msgid "LDAP Directory Servers"
msgstr "خوادم دليل LDAP"

#: directory.label
msgid "Add a directory"
msgstr "أضف دليل"

#: directories.label
msgid ""
"_: do not translate \"LDAP\" in below line\n"
"LDAP Directory Server:"
msgstr "خادم دليل LDAP:"

#: directoriesText.label
#: directoriesText.accesskey
msgid ""
"_: do not translate \"LDAP\" in below line\n"
"&Select an LDAP Directory Server:"
msgstr "انتقِ خادم دليل LDAP:"

#: autocomplete.title
msgid ""
"_: do not translate \"LDAP\" in below line\n"
"LDAP Directory Server"
msgstr "خادم دليل LDAP"

#: addDirectory.label
#: addDirectory.accesskey
msgid "&Add"
msgstr "أ&ضف"

#: editDirectory.label
#: editDirectory.accesskey
msgid "&Edit"
msgstr "&حرّر"

#: deleteDirectory.label
#: deleteDirectory.accesskey
msgid "&Delete"
msgstr "ا&حذف"

