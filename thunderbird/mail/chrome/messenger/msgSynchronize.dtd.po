# translation of msgSynchronize.dtd.po to Arabic
# Khaled Hosny <khaledhosny@eglug.org>, 2007.
# extracted from thunderbird/mail/chrome/messenger/msgSynchronize.dtd
msgid ""
msgstr ""
"Project-Id-Version: msgSynchronize.dtd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-09-13 21:46+0300\n"
"PO-Revision-Date: 2007-09-11 22:45+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\nnplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>= 1 && n%100<=99 ? 4 : 5;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Accelerator-Marker: &\n"

# extracted from MsgSynchronize.xul and msgSelectOffline.xul
#: MsgSynchronize.label
msgid "Download and Sync Messages"
msgstr "نزِّل وزامِن الرسائل"

#: MsgSelect.label
msgid "Items for Offline Use"
msgstr "عناصر للاستخدام عند عدم الاتصال"

#: MsgSyncDesc.label
msgid "If you have already selected mail folders or newsgroups for offline use, you can download and/or sync them now. Otherwise, use the &quot;Select&quot; button to choose mail folders and newsgroups for offline use."
msgstr "إذا كنت قد اخترت مسبقا مجلدات بريد أو مجموعات أخبار لتُستخدم بدون اتصال، يُمكنك تنزيلهم و/أو مزامنتهم الآن. وإلا فاستخدم زر &quot;انتقِ&quot; لتختار المجلدات ومجموعات الأخبار التي تريد استخدامها بدون اتصال."

#: MsgSyncDirections.label
msgid "Download and/or sync the following:"
msgstr "نزِّل و/أو زامن التالي:"

#: syncTypeMail.label
#: syncTypeMail.accesskey
msgid "&Mail messages"
msgstr "رسائل ال&بريد"

#: syncTypeNews.label
#: syncTypeNews.accesskey
msgid "&Newsgroup messages"
msgstr "&مجموعات الأخبار"

#: sendMessage.label
#: sendMessage.accesskey
msgid "&Send Unsent messages"
msgstr "أ&رسِل الرسائل غير المرسلة"

#: workOffline.label
#: workOffline.accesskey
msgid "&Work offline once download and/or sync is complete"
msgstr "ا&عمل بدون اتصال بمجرّد انتهاء التنزيل و/أو المزامنة"

#: selectButton.label
#: selectButton.accesskey
msgid "S&elect..."
msgstr "ا&نتقِ..."

#: MsgSelectDesc.label
msgid "Choose mail folders and newsgroups for offline use."
msgstr "اختر مجلدات بريد ومجموعات الأخبار لتستخدم بدون اتّصال."

#: MsgSelectInd.label
msgid "Download"
msgstr "نزِّل"

#: MsgSelectItems.label
msgid "Folders and Newsgroups"
msgstr "المجلّدات و مجموعات أخبار"

