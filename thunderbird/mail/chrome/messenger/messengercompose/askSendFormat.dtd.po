# translation of askSendFormat.dtd.po to Arabic
# Khaled Hosny <khaledhosny@eglug.org>, 2007.
# extracted from thunderbird/mail/chrome/messenger/messengercompose/askSendFormat.dtd
msgid ""
msgstr ""
"Project-Id-Version: askSendFormat.dtd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-09-13 21:46+0300\n"
"PO-Revision-Date: 2007-09-04 13:00+0300\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Accelerator-Marker: &\n"

# LOCALIZATION NOTE askSendFormat.dtd UI for dialog that asks the user, which format to use for sending a message
#: windowTitle.label
msgid "HTML Mail Question"
msgstr "سؤال عن بريد HTML"

#: recipient.label
msgid "Some of the recipients are not listed as being able to receive HTML mail."
msgstr "بعض المتلقين ليسوا في قائمة من يستطيع استقبال بريد HTML."

#: convertibleDefault.label
msgid "_: convertibleDefault.label\n"
msgstr "_: convertibleDefault.label\n"

#: convertibleYes.label
msgid "Your message can be converted to plain text without losing information."
msgstr "يمكن تحويل رسالتك لنص صِرف بدون فقد أي معلومات."

#: convertibleAltering.label
msgid "Your message can be converted to plain text without losing important information. However, the plain text version might look different from what you saw in the composer."
msgstr "يمكن تحويل رسالتك لنص صِرف دون فقد معلومات مهمة. بالرغم من أن نسخة النص الصِرف ربما تبدو مختلفة عن ما تراه في المحرر ."

#: convertibleNo.label
msgid "However, you used formatting (e.g. colors) that will not be converted to plain text."
msgstr "على الرغم من هذا، لقد استخدمت تنسيق (مثل الألوان) لن يتحول إلى النص صِرف."

#: question.label
msgid "Would you like to convert the message to plain text or send it in HTML anyway?"
msgstr "هل ترغب في تحويل الرسالة إلى نص صِرف أو إرسالها ك‍ ‍HTML على أية حال ؟"

#: plainTextAndHtml.label
msgid "Send in Plain Text and HTML"
msgstr "أرسل نص صِرف وHTML "

#: plainTextOnly.label
msgid "Send in Plain Text Only"
msgstr "أرسِل نص صِرْف فقط"

#: htmlOnly.label
msgid "Send in HTML Only"
msgstr "أرسِل HTML فقط"

#: plainTextAndHtmlRecommended.label
msgid "Send in Plain Text and HTML (recommended)"
msgstr "أرسِل نص صِرْف و HTML (مُفضّل)"

#: plainTextOnlyRecommended.label
msgid "Send in Plain Text Only (recommended)"
msgstr "أرسِل نص صِرف فقط (مفضل)"

#: htmlOnlyRecommended.label
msgid "Send in HTML Only (recommended)"
msgstr "أرسِل HTML فقط (مفضل)"

#: recommended.label
msgid "(recommended)"
msgstr "(مفضل)"

#: send.label
msgid "Send"
msgstr "أرسِل"

#: cancel.label
msgid "Cancel"
msgstr "الغِ"

#: recipients.label
msgid "Recipients..."
msgstr "المستلِمون..."

#: help.label
msgid "Help"
msgstr "مساعدة"

