# translation of printing.properties.po to Arabic
# The contents of this file are subject to the Netscape Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/NPL/
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.#
# The Original Code is mozilla.org code.#
# The Initial Developer of the Original Code is Netscape
# Communications Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.#
# Contributor(s):
#   Roland Mainz <roland.mainz@informatik.med.uni-giessen.de>#
# extracted from /share/translate.org.za/translate.sourceforge.net/xpi-import/firefox-1.0rc1-langenus-linux.xpi:locale/global/printing.properties
# Ayman Hourieh <aymanh@gmail.com>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: printing.properties\n"
"POT-Creation-Date: 2002-07-15 17:13+0100\n"
"PO-Revision-Date: 2004-12-19 21:41+0200\n"
"Last-Translator: Ayman Hourieh <aymanh@gmail.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"

# Page number formating
## @page_number The current page number
#LOCALIZATION NOTE (pageofpages): Do not translate %ld in the following line.
# Place the word %ld where the page number and number of pages should be
# The first %ld will receive the the page number
#: pagenumber
msgid "%1$d"
msgstr "%1$d"

# Page number formating
## @page_number The current page number
## @page_total The total number of pages
#LOCALIZATION NOTE (pageofpages): Do not translate %ld in the following line.
# Place the word %ld where the page number and number of pages should be
# The first %ld will receive the the page number
# the second %ld will receive the total number of pages
#: pageofpages
msgid "%1$d of %2$d"
msgstr "%1$d من %2$d"

# Print font
# The name of the font to be used to print the headers and footers
#: fontname
msgid "serif"
msgstr "serif"

# Print font size
# The size of the font to be used to print the headers and footers
#: fontsize
msgid "10"
msgstr "10"

#: noPrintFilename.title
msgid "Filename is missing"
msgstr "اسم الملفّ مفقود"

#: noPrintFilename.alert
msgid "You have selected \"Print To: File\", and the filename is empty!"
msgstr "انتقيت \"طباعة إلى ملفّ\"، و اسم الملفّ فارغ!"

# File confirm
#: fileConfirm.exists
msgid ""
"%S already exists.\n"
"Do you want to replace it?"
msgstr ""
"%S موجود مسبقاً.\n"
"هل تريد استبداله؟"

# Print error codes
#: print_error_dialog_title
msgid "Printer Error"
msgstr "خطأ طابعة"

#: printpreview_error_dialog_title
msgid "Print Preview Error"
msgstr "خطأ معاينة الطّباعة"

#: NS_ERROR_GFX_PRINTER_CMD_NOT_FOUND
msgid "There was a problem printing. The printer doesn't support a printing command."
msgstr "حدثت مشكلة في الطّباعة. لا تدعم الطّابعة أمر طباعة."

#: NS_ERROR_GFX_PRINTER_CMD_FAILURE
msgid "There was a problem printing. An instruction that was sent to the printer failed."
msgstr "حدثت مشكلة في الطّباعة. فشلت تعليمة أُرسلت إلى الطّابعة."

#: NS_ERROR_GFX_PRINTER_NO_PRINTER_AVAILABLE
msgid "There was a problem printing. No printer could be found."
msgstr "حدثت مشكلة في الطّباعة. لم يمكن إيجاد طابعة."

#: NS_ERROR_GFX_PRINTER_NAME_NOT_FOUND
msgid "There was a problem printing. The printer could not be found."
msgstr "حدثت مشكلة في الطّباعة. لم يمكن إيجاد الطّابعة."

#: NS_ERROR_GFX_PRINTER_ACCESS_DENIED
msgid "There was a problem printing. Access to the printer was denied."
msgstr "حدثت مشكلة في الطّباعة. رفض الوصول إلى الطّابعة."

#: NS_ERROR_GFX_PRINTER_INVALID_ATTRIBUTE
msgid "There was a problem printing. Tried to set an invalid printer attribute."
msgstr "حدثت مشكلة في الطّباعة. تمّت محاولة الوصول لخاصّة طابعة غير صالحة."

#: NS_ERROR_GFX_PRINTER_PRINTER_NOT_READY
msgid "There was a problem printing. The printer not ready."
msgstr "حدثت مشكلة في الطّباعة. الطّابعة غير جاهزة."

#: NS_ERROR_GFX_PRINTER_OUT_OF_PAPER
msgid "There was a problem printing. The printer is out of paper."
msgstr "حدثت مشكلة في الطّباعة. انتهى ورق الطّابعة."

#: NS_ERROR_GFX_PRINTER_PRINTER_IO_ERROR
msgid "There was a problem printing. Printer I/O error."
msgstr "حدثت مشكلة في الطّباعة. خطأ دخل/خرج في الطّابعة."

#: NS_ERROR_GFX_PRINTER_COULD_NOT_OPEN_FILE
msgid "There was a problem printing. The output file could not be opened."
msgstr "حدثت مشكلة في الطّباعة. لم يمكن فتح ملفّ الخرج."

#: NS_ERROR_GFX_PRINTER_FILE_IO_ERROR
msgid "There was an error writing the printing output file."
msgstr "حدث خطأ في كتابة ملفّ خرج الطّباعة."

#: NS_ERROR_GFX_PRINTER_PRINTPREVIEW
msgid "There must be at least one printer available to show Print Preview."
msgstr "يجب أن توجد طابعة واحدة على الأقلّ لعرض معاينة الطّباعة."

#: NS_ERROR_UNEXPECTED
msgid "There was an unexpected problem when printing."
msgstr "حدثت مشكلة غير متوقّعة في الطّباعة."

#: NS_ERROR_OUT_OF_MEMORY
msgid "There was a problem printing. There is not enough free memory to print."
msgstr "حدثت مشكلة في الطّباعة. لا توجد ذاكرة حرّة كافية للطّباعة."

#: NS_ERROR_NOT_IMPLEMENTED
msgid "Some printing functionality is not implemented yet."
msgstr "بعض وظيفيّة الطباعة غير منفّذ."

#: NS_ERROR_NOT_AVAILABLE
msgid "Not available"
msgstr "غير متوفّر"

#: NS_ERROR_ABORT
msgid "The print job was aborted, or canceled."
msgstr "أُحبط أو أُلغي عمل الطّباعة."

#: NS_ERROR_FAILURE
msgid "An unknown error occurred while printing."
msgstr "حدث خطأ غير معروف أثناء الطّباعة."

#: NS_ERROR_GFX_PRINTER_STARTDOC
msgid "Printing failed when starting the document."
msgstr "فشلت الطّباعة عند بدء المستند."

#: NS_ERROR_GFX_PRINTER_ENDDOC
msgid "Printing failed when completing the document."
msgstr "فشلت الطّباعة عند إكمال المستند."

#: NS_ERROR_GFX_PRINTER_STARTPAGE
msgid "Printing failed when starting the page."
msgstr "فشلت الطّباعة عند بدء الصّفحة."

#: NS_ERROR_GFX_PRINTER_ENDPAGE
msgid "Printing failed when completing the page."
msgstr "فشلت الطّباعة عند إنهاء الصّفحة."

#: NS_ERROR_GFX_PRINTER_PRINT_WHILE_PREVIEW
msgid "You cannot print while in print preview."
msgstr "لا يمكنك أن تطبع أثناء معاينة الطّباعة."

#: NS_ERROR_GFX_PRINTER_PAPER_SIZE_NOT_SUPPORTED
msgid "There was a problem printing because the paper size you specified is not supported by your printer."
msgstr "حدثت مشكلة في الطّباعة لأن حجم الورق الذي حدّدته غير مدعوم من طابعتك."

#: NS_ERROR_GFX_PRINTER_ORIENTATION_NOT_SUPPORTED
msgid "There was a problem printing because the page orientation you specified is not supported by your printer."
msgstr "حدثت مشكلة في الطّباعة لأن اتّجاه الورق الذي حدّدته غير مدعوم من طابعتك."

#: NS_ERROR_GFX_PRINTER_COLORSPACE_NOT_SUPPORTED
msgid "There was a problem printing because the print job requires color capabilities that your printer does not support."
msgstr "حدثت مشكلة في الطّباعة لأن عمل الطّباعة يتطلّب إمكانيّات لونيّة غير مدعومة من قبل طابعتك."

#: NS_ERROR_GFX_PRINTER_TOO_MANY_COPIES
msgid "There was a problem printing because you requested too many copies."
msgstr "حدثت مشكلة في الطّباعة لأنك طلبت عدداً كبيراً جدّاً من النسخ."

#: NS_ERROR_GFX_PRINTER_DRIVER_CONFIGURATION_ERROR
msgid "There was a problem printing. The printer driver is not properly configured."
msgstr "حدثت مشكلة في الطّباعة. برنامج قيادة الطّابعة غير معدّ بشكل جيّد."

#: NS_ERROR_GFX_PRINTER_XPRINT_BROKEN_XPRT
msgid "A broken version of the X print server (Xprt) has been detected. Note that printing using this Xprt server may not work properly. Please contact the server vendor for a fixed version."
msgstr "وجد إصدار معطّل من نادل X للطّباعة (Xprt). قد لا يعمل نادل (Xprt) هذا بشكل صحيح. الرّجاء الاتّصال بمصدر النّادل لإصدار مصحّح."

#: NS_ERROR_GFX_PRINTER_DOC_IS_BUSY_PP
msgid ""
"The browser cannot print preview right now.\n"
"Please try again when the page has finished loading."
msgstr ""
"لا يستطع المتصفّح معاينة الطّباعة الآن.\n"
"الرّجاء المحاولة مرّة ثانية عند انتهاء تحميل الصّفحة."

#: NS_ERROR_GFX_PRINTER_DOC_WAS_DESTORYED
msgid ""
"The page was replaced while you were trying to print.\n"
"Please try again."
msgstr ""
"استبدلت الصّفحة أثناء محاولتك طباعتها.\n"
"الرّجاء المحاولة مرّة أخرى."

#: NS_ERROR_GFX_NO_PRINTDIALOG_IN_TOOLKIT
msgid ""
"Either pluggable dialogs were not properly installed\n"
"Or this GFX Toolkit no longer supports native Print Dialogs"
msgstr ""
"إمّا أن الحوارات القابلة للإلحاق غير مثبّتة بشكل جيّد\n"
"أو أنّ عدّة GFX لا تدعم الحوارات الأصليّة بعد الآن"

#: NS_ERROR_GFX_NO_PRINTROMPTSERVICE
msgid "The Printing Prompt Service is missing."
msgstr "خدمة طلب الطّباعة مفقودة."

#: NS_ERROR_GFX_PRINTER_XPRINT_NO_XPRINT_SERVERS_FOUND
msgid ""
"There was a problem printing. No Xprint server(s) could be found.\n"
"Check whether the XPSERVERLIST environment variable contains any valid Xprint servers.\n"
"For further information see http://xprint.mozdev.org/ or http://www.mozilla.org/projects/xprint/"
msgstr ""
"حدثت مشكلة في الطّباعة. لم يمكن إيجاد أي نوادل Xprint.\n"
"إفحص للتأكّد أنّ متحوّل البيئة XPSERVERLIST يحتوي نوادل Xprint صالحة.\n"
"للمزيد من المعلومات إنظر http://xprint.mozdev.org/ أو http://www.mozilla.org/projects/xprint/"

#: NS_ERROR_GFX_PRINTER_PLEX_NOT_SUPPORTED
msgid "There was a problem printing because the plex mode you specified is not supported by your printer."
msgstr "حدثت مشكلة في الطّباعة لأنّ وضع بليكس الذي حدّدته غير مدعوم من طابعتك."

#: NS_ERROR_GFX_PRINTER_DOC_IS_BUSY
msgid "The browser cannot print the document while it is being loaded."
msgstr "لا يستطيع المتصفّح أن يطبع المستند أثناء تحميله."

#: NS_ERROR_GFX_PRINTING_NOT_IMPLEMENTED
msgid "Printing is not implemented."
msgstr "الطّباعة غير منفّذة."

#: NS_ERROR_GFX_COULD_NOT_LOAD_PRINT_MODULE
msgid "The requested print module cannot be loaded."
msgstr "لا يمكن تحميل وحدة الطّباعة المطلوبة."

# No printers available
#: noprinter
msgid "No printers available."
msgstr "لا تتوفّر طابعات."

#: PrintToFile
msgid "Print To File"
msgstr "الطّباعة إلى ملف"

