# translation of accessible.properties.po to Arabic
# extracted from /share/translate.org.za/translate.sourceforge.net/xpi-import/firefox-1.0rc1-langenus-linux.xpi:locale/global/accessible.properties
# Abdulaziz Al-Arfaj <alarfaj0@yahoo.com>, 2004.
# Ayman Hourieh <aymanh@gmail.com>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: accessible.properties\n"
"POT-Creation-Date: 2002-07-15 17:13+0100\n"
"PO-Revision-Date: 2004-12-19 21:09+0200\n"
"Last-Translator: Ayman Hourieh <aymanh@gmail.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 0 : n==2 ? 1 : n>=3 && n<=10 ? 2 : 3\n"

#: jump
msgid "Jump"
msgstr "قفز"

#: press
msgid "Press"
msgstr "ضغط"

#: check
msgid "Check"
msgstr "اختيار"

#: uncheck
msgid "Uncheck"
msgstr "إزالة الاختيار"

#: select
msgid "Select"
msgstr "انتقاء"

#: open
msgid "Open"
msgstr "فتح"

#: close
msgid "Close"
msgstr "إغلاق"

#: switch
msgid "Switch"
msgstr "تحويل"

#: click
msgid "Click"
msgstr "نقر"

#: collapse
msgid "Collapse"
msgstr "تقليص"

#: expand
msgid "Expand"
msgstr "توسيع"

