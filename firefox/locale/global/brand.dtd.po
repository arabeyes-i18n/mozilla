# translation of brand.dtd.po to Arabic
# extracted from /share/translate.org.za/translate.sourceforge.net/xpi-import/firefox-1.0rc1-langenus-linux.xpi:locale/global/brand.dtd
# Abdulaziz Al-Arfaj <alarfaj0@yahoo.com>, 2004.
# Ayman Hourieh <aymanh@gmail.com>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: brand.dtd\n"
"POT-Creation-Date: 2002-07-15 17:13+0100\n"
"PO-Revision-Date: 2004-12-15 19:20+0200\n"
"Last-Translator: Ayman Hourieh <aymanh@gmail.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 0 : n==2 ? 1 : n>=3 && n<=10 ? 2 : 3\n"

#: brandShortName
msgid "Firefox"
msgstr "فايرفوكس"

#: brandFullName
msgid "Mozilla Firefox"
msgstr "موزيلا فايرفوكس"

#: vendorShortName
msgid "Mozilla"
msgstr "موزيلا"

#: releaseURL
msgid "http://www.mozilla.org/products/firefox/releases/1.0.html"
msgstr "http://www.mozilla.org/products/firefox/releases/1.0.html"

