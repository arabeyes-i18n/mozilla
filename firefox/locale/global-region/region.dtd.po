# translation of region.dtd.po to Arabic
# extracted from /share/translate.org.za/translate.sourceforge.net/xpi-import/firefox-1.0rc1-langenus-linux.xpi:locale/global-region/region.dtd
# Ayman Hourieh <aymanh@gmail.com>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: region.dtd\n"
"POT-Creation-Date: 2002-07-15 17:13+0100\n"
"PO-Revision-Date: 2004-10-30 12:35+0200\n"
"Last-Translator: Ayman Hourieh <aymanh@gmail.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"

#: vendorURL
# brand.dtd
msgid "http://www.mozilla.org/"
msgstr "http://www.mozilla.org/"

#: releaseURL
msgid "http://www.mozilla.org/releases/"
msgstr "http://www.mozilla.org/releases/"

#: getNewThemesURL
msgid "http://mozilla.org/themes/download/"
msgstr "http://mozilla.org/themes/download/"

#: content.version
msgid "1.5a"
msgstr "1.5a"

#: locale.dir
msgid "ltr"
msgstr "rtl"

