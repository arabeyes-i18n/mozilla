# translation of update.properties.po to Arabic
# extracted from /share/translate.org.za/translate.sourceforge.net/xpi-import/firefox-1.0rc1-langenus-linux.xpi:locale/mozapps/update/update.properties
# Ayman Hourieh <aymanh@gmail.com>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: update.properties\n"
"POT-Creation-Date: 2002-07-15 17:13+0100\n"
"PO-Revision-Date: 2004-12-19 22:09+0200\n"
"Last-Translator: Ayman Hourieh <aymanh@gmail.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"

#: mismatchCheckNow
msgid "Check Now"
msgstr "الفحص الآن"

#: mismatchCheckNowAccesskey
#: cancelButtonTextAccesskey
msgid "C"
msgstr "ف"

#: mismatchDontCheck
msgid "Don't Check"
msgstr "لا تفحص"

#: mismatchDontCheckAccesskey
msgid "D"
msgstr "ل"

#: installButtonText
msgid "Install Now"
msgstr "التّثبيت الآن"

#: installButtonTextAccesskey
msgid "I"
msgstr "ت"

#: nextButtonText
msgid "Next >"
msgstr "التّالي"

#: nextButtonTextAccesskey
msgid "N"
msgstr "ت"

#: cancelButtonText
msgid "Cancel"
msgstr "إلغاء"

#: app.update.url
msgid "https://update.mozilla.org/update/firefox/en-US.rdf"
msgstr "https://update.mozilla.org/update/firefox/ar-JO.rdf"

#: updatesAvailableTitle
msgid "New Updates Available"
msgstr "تتوفّر تحديثات جديدة"

#: updatesAvailableText
msgid "Click Here to View"
msgstr "إنقر هنا للعرض"

#: checkingPrefix
msgid "Checking for Updates to %S ..."
msgstr "جاري الفحص عن تحديثات لـ %S ..."

#: downloadingPrefix
msgid "Downloading: %S"
msgstr "جاري التّنزيل: %S"

#: installingPrefix
msgid "Installing: %S"
msgstr "جاري التّثبيت: %S"

#: closeButton
msgid "Close"
msgstr "إغلاق"

#: installErrorDescription
msgid "The following components could not be installed due to errors (the file could not be downloaded, was corrupt, or for some other reason)."
msgstr "لم يمكن تثبيت المكوّنات التّالية بسبب أخطاء (لم يمكن تنزيل الملفّ، كان معطوباً، أو لسبب آخر)."

#: checkingErrorDescription
msgid "%S could not check for updates to the following components (either the update server(s) did not respond, or the update service(s) were not found)."
msgstr "لم يستطع %S الفحص عن تحديثات للمكوّنات التّالية (لم يستجب نادل(نوادل) التّحديث، أو لم توجد خدمة(ات) التّحديث)."

#: installErrorItemFormat
msgid "%S (%S)"
msgstr "%S (%S)"

#: versionUpdateComplete
msgid "Version Compatibility Update Complete"
msgstr "اكتمال تحديث توافقيّة الإصدار"

#: updatesAvailableTooltip-0
#: updatesAvailableTooltip-1
msgid "Update(s) Available"
msgstr "تحديث(ات) متوفّر(ة)"

#: updatesAvailableTooltip-2
msgid "Critical Update(s) Available"
msgstr "تحديث(ات) حرج(ة) متوفّر(ة)"

#: updatesCheckForUpdatesTooltip
msgid "Double-click here to check for updates"
msgstr "إنقر مرّتين هنا للفحص عن تحديثات"

#: installTextNoFurtherActions
#: foundInstructions
msgid "Choose the ones you want to install and click Install Now to install them."
msgstr "إختر ما تريد تثبيته و إنقر تثبيت الآن لتثبيتها."

#: installTextFurtherCations
#: foundInstructionsAppComps
msgid "Choose the ones you want to install and click Next to continue."
msgstr "إختر ما تريد تثبيته و إنقر التّالي للمتابعة."

#: appNameAndVersionFormat
msgid "%S %S"
msgstr "%S %S"

#: updateTypePatches
msgid "Critical Updates (%S)"
msgstr "تحديثات حرجة (%S)"

#: updateTypeComponents
msgid "Optional Components (%S)"
msgstr "مكوّنات اختيارية (%S)"

#: updateTypeExtensions
msgid "Extensions and Themes (%S)"
msgstr "امتدادات و تيم (%S)"

#: updateTypeLangPacks
msgid "Language Packs (%S)"
msgstr "حزم لغات (%S)"

#: foundAppLabel
msgid "%S %S is available. We strongly recommend that you install this upgrade as soon as possible."
msgstr "%S %S متوفّر. ننصح بشدّة بتثبيت هذه التّرقية في أقرب وقت."

#: foundAppFeatures
msgid "%S %S features:"
msgstr "مزايا %S %S:"

#: foundAppInfoLink
msgid "More information about %S %S ..."
msgstr "المزيد من المعلومات عن %S %S ..."

#: appupdateperformedtitle
msgid "Restart Required"
msgstr "إعادة البدء مطلوبة"

#: appupdateperformedmessage
msgid "%S has been updated this session. Please restart %S before performing any more updates."
msgstr "تمّ تحديث %S في هذه الجّلسة. الرّجاء إعادة بدء %S قبل إجراء المزيد من التّحديثات."

#: updatesdisabledtitle
msgid "Update Disabled"
msgstr "التّحديث معطّل"

#: updatesdisabledmessage
msgid "Update has been disabled by your Administrator."
msgstr "التّحديث معطّل من قبل المدير."

