# translation of help.dtd.po to Arabic
# Khaled Hosny <khaledhosny@eglug.org>, 2007.
# extracted from en/toolkit/chrome/mozapps/help/help.dtd, ar/toolkit/chrome/mozapps/help/help.dtd
msgid ""
msgstr ""
"Project-Id-Version: help.dtd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-07-26 16:46+0300\n"
"PO-Revision-Date: 2007-07-23 02:37+0300\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Generator: KBabel 1.11.4\n"

#: printCmd.commandkey
msgid "p"
msgstr "p"

#: findOnCmd.commandkey
msgid "F"
msgstr "F"

#: findAgainCmd.commandkey
msgid "G"
msgstr "G"

#: findAgainCmd.commandkey2
msgid "VK_F3"
msgstr "VK_F3"

#: backButton.label backButton.accesskey
msgid "&Back"
msgstr "إلى ال&خلف"

#: backButton.tooltip
msgid "Go back one page"
msgstr "عُد للخلف صفحة واحدة"

#: forwardButton.label forwardButton.accesskey
msgid "&Forward"
msgstr "إلى الأ&مام"

#: forwardButton.tooltip
msgid "Go forward one page"
msgstr "اذهب للأمام صفحة واحدة"

#: copyCmd.label copyCmd.accesskey
msgid "&Copy"
msgstr "ان&سخ"

#: selectAllCmd.label selectAllCmd.accesskey
msgid "Select &All"
msgstr "انتقِ ال&كل"

#: goBackCmd.commandkey
msgid "["
msgstr "["

#: goForwardCmd.commandkey
msgid "]"
msgstr "]"

#: homeButton.label
msgid "Home"
msgstr "الصّفحة الرّئيسيّة"

#: homeButton.tooltip
msgid "Go to the Help Start Page"
msgstr "اذهب إلى صفحة بدء المساعدة"

#: printButton.label
msgid "Print"
msgstr "اطبع"

#: printButton.tooltip
msgid "Print this page"
msgstr "اطبع هذه الصّفحة"

#: closeWindow.commandkey
msgid "W"
msgstr "W"

#: throbberItem.title
msgid "Activity Indicator"
msgstr "مؤشّر النّشاط"

#: throbber.tooltip
msgid "Go to the Help on Help section"
msgstr "اذهب إلى مساعدة قسم المساعدة"

#: searchtab.label searchtab.accesskey
msgid "&Search"
msgstr "ا&بحث"

#: toctab.label toctab.accesskey
msgid "&Contents"
msgstr "&المحتويات"

#: textZoomReduceCmd.commandkey
msgid "-"
msgstr "-"

#: textZoomEnlargeCmd.commandkey
msgid "+"
msgstr "+"

# + is above this key on many keyboards
#: textZoomEnlargeCmd.commandkey2
msgid "="
msgstr "="

#: textZoomResetCmd.commandkey
msgid "0"
msgstr "0"

#: helpSearch.commandkey
msgid "k"
msgstr "k"

#: zLevel.label zLevel.accesskey
msgid "Always on &Top"
msgstr "دائما &أعلى"

#: textZoomReduceBtn.label textZoomReduceBtn.accesskey
msgid "&Decrease Text Size"
msgstr "&صغّر حجم النص"

#: textZoomEnlargeBtn.label textZoomEnlargeBtn.accesskey
msgid "&Increase Text Size"
msgstr "&زِد حجم النص"

