# translation of pageReport.dtd.po to Arabic
# extracted from ar/browser/chrome/browser/pageReport.dtd
# Ayman Hourieh <aymanh@gmail.com>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: pageReport.dtd\n"
"POT-Creation-Date: 2005-04-23 12:48\n"
"PO-Revision-Date: 2005-04-23 17:51+0200\n"
"Last-Translator: Ayman Hourieh <aymanh@gmail.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"

#: caption.label
msgid "Blocked Popups"
msgstr "النّوافذ المنبثقة المصدودة"

#: intro.label
msgid ""
"The following pages were prevented from displaying "
"unrequested popup windows:"
msgstr ""
"مُنعت الصّفحات التّالية من عرض "
"نوافذ منبثقة غير مطلوبة:"

#: done.label
#: done.accesskey
msgid "&Done"
msgstr "&تمّ"

#: unblock.label
#: unblock.accesskey
msgid "&Unblock Site"
msgstr "إ&لغاء صدّ الموقع"

