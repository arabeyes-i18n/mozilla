# translation of metaData.dtd.po to Arabic
# extracted from ar/browser/chrome/browser/metaData.dtd
# Ayman Hourieh <aymanh@gmail.com>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: metaData.dtd\n"
"POT-Creation-Date: 2005-04-23 12:48\n"
"PO-Revision-Date: 2005-04-23 17:37+0200\n"
"Last-Translator: Ayman Hourieh <aymanh@gmail.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"

#: no-properties.label
msgid "No properties set."
msgstr "لم تضبط أيّة خصائص."

#: caption.label
msgid "Element Properties"
msgstr "خصائص العناصر"

#: image-sec.label
msgid "Image Properties"
msgstr "خصائص الصّور"

#: image-url.label
msgid "Location:"
msgstr "الموقع:"

#: image-desc.label
msgid "Description:"
msgstr "الوصف:"

#: image-alt.label
msgid "Alternate text:"
msgstr "النّصّ البديل:"

#: image-width.label
msgid "Width:"
msgstr "العرض:"

#: image-height.label
msgid "Height:"
msgstr "الارتفاع:"

#: image-filesize.label
msgid "Size of File:"
msgstr "حجم الملفّ:"

#: insdel-sec.label
msgid "Insert/Delete Properties"
msgstr "إدخال/حذف الخصائص"

#: insdel-cite.label
msgid ""
"_: insdel-cite.label\n"
"Info:"
msgstr "معلومات:"

#: insdel-date.label
msgid "Date:"
msgstr "التّاريخ:"

#: link-sec.label
msgid "Link Properties"
msgstr "خصائص الوصلة"

#: link-url.label
msgid "Address:"
msgstr "العنوان:"

#: link-target.label
msgid "Will open in:"
msgstr "ستفتح في:"

#: link-type.label
msgid "Target type:"
msgstr "نوع الهدف:"

#: link-lang.label
msgid "Target language:"
msgstr "لغة الهدف:"

#: link-rel.label
msgid "Relation:"
msgstr "العلاقة:"

#: link-rev.label
msgid "Reversed relation:"
msgstr "معكوس العلاقة:"

#: misc-sec.label
msgid "Miscellaneous Properties"
msgstr "خصائص متفرّقة"

#: misc-lang.label
msgid "Text language:"
msgstr "لغة النّصّ:"

#: misc-title.label
msgid "Title:"
msgstr "العنوان:"

#: misc-tblsummary.label
msgid "Table summary:"
msgstr "ملخّص الجدول:"

#: quote-sec.label
msgid "Quote Properties"
msgstr "خصائص الاقتباس"

#: quote-cite.label
msgid ""
"_: quote-cite.label\n"
"Info:"
msgstr "معلومات:"

