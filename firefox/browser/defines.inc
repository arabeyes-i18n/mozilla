#filter emptyLines

#define MOZ_LANGPACK_EID {3D1564DC-4ECF-11D9-9F5E-DAE4390B220D}
#define MOZ_LANGPACK_CREATOR Arabeyes.org

# If non-English locales wish to credit multiple contributors, uncomment this
# variable definition and use the format specified.
#define MOZ_LANGPACK_CONTRIBUTORS <em:contributor>Ayman Hourieh</em:contributor> <em:contributor>Abdulaziz Al-Arfaj</em:contributor>

#unfilter emptyLines
