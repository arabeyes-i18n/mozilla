#filter emptyLines

# This file is in the UTF-8 encoding

# Installer keys (don't translate strings in $$)
#define SETUP_TITLE تثبيت $ProductShortName$
#define SETUP_WELCOME أهلا بكم في %s
#define SETUP_MESSAGE0 أنت على وشك تثبيت %s $Version$.
#define SETUP_MESSAGE1 ينصح بشدّة الخروج من كل برامج ويندوز قبل تشغيل برنامج التنصيب هذا.
#define SETUP_MESSAGE1_UNIX ينصح بشدّة الخروج من كل تواجدات %s قبل تشغيل برنامج التنصيب هذا.
#define SETUP_MESSAGE2 إنقر إلغاء للخروج من التنصيب و أغلق أي برامج تعمل.  إنقر التّالي لمتابعة برنامج التنصيب.
#define SETUP_MESSAGE3 إنقر التّالي لمتابعة تثبيت %s $Version$.

#define LICENSE_TITLE اتّفاقية ترخيص البرنامج
#define LICENSE_SUBTITLE شروط استخدام هذا البرنامج.
#define LICENSE_MESSAGE0 الرّجاء قراءة اتّفاقية الترخيص التّالية. استخدم الشريط لقراءة الباقي من هذه الاتّفاقية.
#define LICENSE_ACCEPT أوا&فق على شروط هذه الاتّفاقية
#define LICENSE_DECLINE &لا أوافق على شروط هذه الاتّفاقية

#define SETUP_TYPE_TITLE نوع التّنصيب
#define SETUP_TYPE_SUBTITLE اختر خيارات التّنصيب.
#define SETUP_TYPE_MESSAGE0 إنتقِ نوع التّنصيب الذي تفضّله، ثم إنقر التّالي.
#define SETUP_TYPE_EXISTING يحوي الدليل المنتقى فايرفوكس منصّباً. لحذف %s بشكل كامل، رجاءً إنقر زر 'حذف الدّليل'. لن تتأثّر إعدادات  فايرفوكس. أو، رجاءً إنقر زر 'إلغاء' و إختر دليل هدف آخر.

# &Ampersand is used to select a shortcut key
#define TYPE_STANDARD &قياسي
#define TYPE_STANDARD_DESC يتمّ تثبيت المتصفّح بالإعدادات الشّائعة.

#define TYPE_CUSTOM &مخصّص
#define TYPE_CUSTOM_DESC يمكنك اختيار خيارات التّنصيب بشكل مفرد. ينصح به للمستخدمين المتقدّمين.

#define SELECT_TITLE اختيار المكوّنات
#define SELECT_SUBTITLE مكوّنات إضافيّة تحسّن $ProductShortName$.
#define SELECT_MESSAGE0 اختر المكوّنات الإضافيّة التي تريد تنصيبها، ثمّ إنقر التّالي.

#define LOCATION_TITLE مجلّد التّنصيب
#define LOCATION_SUBTITLE مكان تثبيت $ProductShortName$.
#define LOCATION_MESSAGE0 سيتمّ تثبيت $ProductShortName$ في المجلّد التّالي:

#define UPGRADE_TITLE تحديث
#define UPGRADE_SUBTITLE تحديث تثبيت موجود من $ProductShortName$.
#define UPGRADE_CLEANUP وجد تثبيت سابق من $ProductNameInternal$ في المجلّد المنتقى.

#define UPGRADE_CLEAN إجراء تثبيت نظيف
#define UPGRADE_CLEAN_MSG سيحذف التنصيب النظيف بشكل كامل محتويات مجلّد التّنصيب! ستخسر أي مكونات إضافية في هذا المجلّد. ينصح به إذ أنّه قد يمنع عدم التّوافق. (XXXben)
#define UPGRADE_OVER إذا اخترت عدم إجراء تثبيت نظيف، ستبقى المكوّنات الإضافية، مما قد يسبّب سلوكاً غير متوقّع.
#define UPGRADE_OVER_WINDIR لا يمكن إجراء تحديث آمن من $ProductNameInternal$ لأنّ البرنامج مثبّت في مجلّد ويندوز. ينصح بختيار مجلّد آخر لتثبيت $ProductNameInternal$.

#define ADD_TITLE تثبيت $ProductShortName$ - اختيار مكوّنات إضافيّة
#define ADD_MESSAGE0 اختر أو ألغِ اختيار مكوّنات إضافيّة ليتمّ تثبيتها.

#define WININT_TITLE إعداد الاختصارات
#define WININT_SUBTITLE إنشاء أيقونات البرنامج
#define WININT_MESSAGE0 ينشئ أيقوات لـ $ProductShortName$:

#define WININT_DESKTOP على سطح المكتب
#define WININT_START في مجلّد البرامج ضمن إبدأ
#define WININT_QUICKL في شريط التشغيل السّريع

#define USE_AS_DEFAULT استخدم %s كمتصفّحي الافتراضي.

#define WININT_PFOLDER_TITLE تثبيت $ProductShortName$ - اختيار مجلّد البرنامج
#define WININT_PFOLDER_MESSAGE0 سيضيف برنامج التّثبيت أيقونات البرنامج إلى المجلّد المدرج أسفلاً. يمكنك كتابة اسم مجلّد جديد، أو اخيار واحد من قائمة المجلّدات الموجودة. إنقر التّالي للمتابعة.

#define ADDL_OPTIONS_TITLE تثبيت $ProductShortName$ - خيارات إضافيّة
#define ADDL_OPTIONS_MSG1 انتقِ الخيار التّالي إذا أردت حفظ ملفّات التثبيت المنزّلة على الكمبيوتر.  يسمح لك هذا بإعادة تشغيل برنامج التثبيت دون تنزيل أي ملفّات.  ستحفظ ملفّات التّثبيت في المسار التّالي.

#define ADV_SETTINGS_TITLE تثبيت $ProductShortName$ - إعدادات متقدّمة
#define ADV_SETTINGS_MSG إذا كان اتّصال انترنت يتطلّب نادل بروكسي، أدخل معلومات اسم النادل و المنفذ.  إذا كان اتّصال انترنت لا يتطلب بروكسي، إترك الحقول فارغة.

#define START_TITLE الملخّص
#define START_SUBTITLE جاهز لتثبيت $ProductShortName$.
#define START_INSTALL سيثبّت برنامج التّثبيت المكوّنات التّالية:
#define START_DOWNLOAD سينزّل و يثبّت برنامج التّثبيت المكوّنات التّالية:
#define START_MESSAGE0 إنقر التّالي للمتابعة.

#define DL_TITLE جاري التّنزيل
#define DL_SUBTITLE جاري تنزيل المكوّنات المطلوبة...
#define DL_BLURB يقوم برنامج التّثبيت بتنزيل المكوّنات المطلوبة لتثبيت $ProductShortName$.
#define DL_FILENAME حاليّا جاري تنزيل:
#define DL_TIMELEFT الوقت المتبقّي:

#define INSTALL_TITLE جاري تثبيت
#define INSTALL_SUBTITLE جاري تثبيت $ProductShortName$...
#define INSTALL_BLURB يقوم برنامج التّثبيت بتثبيت ملفّات التّطبيق.
#define INSTALL_STATUSFILE جاري إعداد الملف:
#define INSTALL_STATUSCOMP حاليّا جاري تثبيت:

#define COMPLETE_TITLE تم التّثبيت
#define COMPLETE_MESSAGE0 تمّ تثبيت %s $Version$ بنجاح.
#define COMPLETE_MESSAGE1 إنقر إنهاء لإكمال التّثبيت.
#define COMPLETE_LAUNCH بدء %s $Version$ الآن.
#define COMPLETE_RESET_HOMEPAGE استخدام صفحة بدء فايرفوكس كالصفحة الرّئيسيّة.

#define DL2_TITLE تثبيت $ProductShortName$ - تنزيل
#define DL2_MESSAGE0 ينزّل برنامج التثبيت الملفّات المطلوبة الآن. قد يستغرق هذا بعض الوقت تبعا لسرعة الاتّصال عندك.
#define DL2_RETRY فشل فحص CRC على بعض الملفّات و ستنزّل مرة ثانية. سيتمّ تنزيل الملفات التي فشلت فقط مرّة ثانية.

#define CLOSE_CHECK نجح تنزيل $ProductShortName$. يجب إغلاق $ProductNameInternal$ لمتابعة التّثبيت. إنقر موافق للخروج من $ProductNameInternal$ أليّا و بدء التّثبيت.
#define CLOSE_CHECK2 يجب إغلاق $ProductNameInternal$ لمتابعة التّثبيت. إنقر موافق لإغلاق $ProductNameInternal$ آليّا و بدء التّثبيت.
#define CLOSE_WAIT جاري إغلاق $ProductNameInternal$.  الرّجاء الانتظار...

#define XPCOM_SHORT COM مستقلة عن المنصّة
#define BROWSER_SHORT متصفّح $ProductShortName$
#define HELP_SHORT مساعدة $ProductShortName$
#define UNINSTALL_SHORT مزيل تثبيت $CompanyName$
#define LANGPACK_TITLE حزمة لغة

#define ADT_SHORT أدوات التّطوير
#define ADT_LONG متحرّي المستند - أداة لمطوّري ويب.

#define QFA_SHORT عميل الإعلام بالجودة
#define QFA_LONG للإعلام بمعلومات انهيار $ProductShortName$

#define RPT_SHORT مبلّغ مواقع ويب
#define RPT_LONG للإبلاغ عن مشاكل في توافق المواقع مع $ProductShortName$

#define CORE_PREPARING جاري تحضير التّثبيت، الرّجاء الانتظار...
#define INST_UNINST جاري تثبيت مزيل التّثبيت، الرّجاء الانتظار...

#define MSG_UNFINISHED_DL لم تنتهِ جلسة سابقة من تنزيل كلّ الملفّات الضروريّة. هل تريد استخدام الملفّات المنزّلة مسبقا، لتجنّب تنزيلها مرّة ثانية؟
#define MSG_UNFINISHED_INSTALL لم تنتهِ جلسة سابقة من تثبيت كلّ الملفّات الضروريّة. هل تريد استخدام الملفّات المنزّلة مسبقا، لتجنّب تنزيلها مرّة ثانية؟
#define MSG_CORRUPTED وجد برنامج التّثبيت أنّ ملفّات التّثبيت التّالية تالفة:%sعليك الحصول على نسخة ثانية من برنامج تثبيت $ProductShortName$ لمتابعة التّثبيت.
#define MSG_CORRUPTED_AUTO وجد برنامج التّثبيت أنّ واحدا على الاقل كم ملفّات التّثبيت تالفة. عليك الحصول على نسخة ثانية من برنامج تثبيت  $ProductShortName$ متابعة التّثبيت.
#define MSG_TOO_MANY_CRC فشل التّنزيل عددا كبيرا من المرّات.  سيتمّ إلغاء التّثبيت.  لن تحذف أيّ ملفّات تمّ تنزيلها. عند تشغيل برنامج التّثبيت مرّة ثناية، سيتمّ أخذ هذه الملفّات بعين الاعتبار.
#define MSG_TOO_MANY_NETWORK حدثت أخطاء شبكة كثيرة عند تنزيل %s.  سيتوقّف برنامج التّثبيت مؤقّتا الآن. الرّجاء نقر متابعة لإعادة محاولة تنزيل الملفّات.
#define MSG_VERIFYING جاري التحقّق من تكامل الملفّات المضغوطة، الرّجاء الانتظار...
#define MSG_UNDEFINED خطأ غير محدّد
#define MSG_OOM نفذت الذّاكرة!
#define MSG_STATUS_DL %s عند %.2f ك.ب./ثانية (تمّ تنزيل%u ك.ب. من %u ك.ب.)
#define MSG_COMPLETED إكتمل %d%%
#define MSG_REQUIRED (مطلوب)
#define MSG_CANCELING جاري إلغاء التّثبيت، الرّجاء الانتظار...
#define MSG_ERROR_UNCOMPRESS خطأ في فكّ ضغط الملف %s: %d
#define MSG_USAGE Usage: %s [options]\n	[options] can be any of the following combination:\n		-h: This help.\n		-a [path]: Alternate archive search path.\n		-app [app id]: ID of application which is launching the installer (shared installs)\n		-app_path [app]: Points to (full path) representative file of application (Shared installs)\n	*	-dd [path]: Suggested install destination directory. (Shared installs)\n	*	-greLocal: Forces GRE to be installed into the application dir.\n	*	-greShared: Forces GRE to be installed into a global, shared dir (normally)\n			c:\program files\common files\mozilla.org\GRE\n		-reg_path [path]: Where to make entries in the Windows registry. (Shared installs)\n		-f: Force install of GRE installer (Shared installs), though it'll work\n			for non GRE installers too.\n		-greForce: Force 'Component GRE' to be downloaded, run, and installed.  This\n			bypasses GRE's logic of determining when to install by running its\n			installer with a '-f' flag.\n		-n [filename]: Setup's parent process filename.\n	*	-ma: Run setup in Auto mode.\n	*	-ms: Run setup in Silent mode.\n		-ira: Ignore the [RunAppX] sections\n		-ispf: Ignore the [Program FolderX] sections that show\n			the Start Menu shortcut folder at the end of installation.\n	*	-showBanner: Show the banner image in the download and install progress dialogs\n	*	-hideBanner: Hide the banner image in the download and install progress dialogs\n	*	-cleanupOnUpgrade: Tells Setup to check to see if user is upgrading (installing on top\n		of previous version of product).  If user is upgrading:\n			* NORMAL mode: prompt user on how to proceed\n			* All other modes: assume user wants to cleanup.\n	*	-noCleanupOnUpgrade: Tells Setup to not check if user is upgrading (installing on top\n		of previous version of product).  This will disable the cleanup feature.\n\n	* means it will override config.ini
#define MSG_USE_FTP استخدام &FTP لتنزيل الملفّات
#define MSG_USE_HTTP استخدام &HTTP لتنزيل الملفّات
#define MSG_SAVE_LOCALLY &حفظ ملفات التّثبيت محليّا
#define MSG_DL_PAUSED واجه برنامج التّثبيت مشكلة في الشّبكة و أوقف التّنزيل.  إذا كنت قد فقدت اتصال الشبكة، الرّجاء نقر متابعة عند استعادة الاتّصال الشّبكي.
#define MSG_NOT_ADMIN وجد برنامج التّنزيل أنّك لا تملك صلاحيّات مدير النّظام.  ينصح بشدّة عدم متابعة تثبيت $ProductShortName$، لأنّه قد لا يعمل بشكل صحيح.  هل لازلت تريد المتابعة؟
#define MSG_NOT_ADMIN_AUTO وجد برنامج التّنزيل أنّك لا تملك صلاحيّات مدير النّظام.  لا يمكن لبرنامج التّثبيت متابعة تثبيت $ProductShortName$.

#define UNINST_RUNNING وجد أن $ProductNameInternal$ يعمل حاليا.  الرّجاء الخروج من $ProductNameInternal$ قبل المتابعة.  إنقر موافق للخروج من $ProductNameInternal$ آليا و متابعة التّثبيت.
#define UNINST_SHUTDOWN جاري إغلاق $ProductNameInternal$.  الرّجاء الانتظار...

#define UNINST_FONT MS Sans Serif
#define UNINST_FONTSIZE 8
#define UNINST_CHARSET 0
# Here is a partial list CHAR_SETS
#  ANSI_CHARSET = 0
#  DEFAULT_CHARSET = 1
#  SYMBOL_CHARSET = 2
#  SHIFTJIS_CHARSET = 128
#  GB2312_CHARSET = 134
#  HANGEUL_CHARSET = 129
#  CHINESEBIG5_CHARSET = 136
#  OEM_CHARSET 255

#define UNINST_TITLE مزيل تثبيت $ProductName$
#define UNINST_MESSAGE0 هل أنت متأكد أنك تريد إزالة %s و كل مكوناته؟
#define BTN_UNINST إ&زالة تثبيت
#define BTN_CANCEL إل&غاء
#define BTN_MESSAGE1 وجد برنامج إزالة التّثبيت أن الملفّات المشتركة التّالية غير مستخدمة من قبل برامج أخرى.  إذا كانت برامج أخرى لاتزال تحتاج الملف المشترك و تمّت إزالته، قد يتوقّف هذا البرنامج عن العمل.  هل أنت متأكّد أنّك تريد إزالة هذا الملف المشترك؟
#define BTN_MESSAGE2 لن يضرّ ترك هذا الملف بنظامك.  إذا لم تكن متأكّدا، ينصح بعدم إزالة الملفّ المشترك من النّظام.
#define BTN_FILENAME اسم الملفّ:
#define BTN_NO &لا
#define BTN_NOTOALL ل&ا للكلّ
#define BTN_YES &نعم
#define BTN_YESTOALL ن&عم للكلّ

#define ERROR_DLL_LOAD لم يمكن تحميل %s
#define ERROR_STRING_LOAD لم يمكن تحميل هويّة مصدر السّلسلة %d
#define ERROR_STRING_NULL وجد مؤشر خالي.
#define ERROR_GLOBALALLOC خطأ تخصيص ذاكرة.
#define ERROR_FAILED فشل %s.
#define ERROR_DIALOG_CREATE لم يمكن إنشاء حوار %s.
#define DLGQUITTITLE سؤال
#define DLGQUITMSG هل أنت متأكّد أنّك تريد الإلغاء
#define ERROR_GET_SYSTEM_DIRECTORY_FAILED فشل GetSystemDirectory().
#define ERROR_GET_WINDOWS_DIRECTORY_FAILED فشل GetWindowsDirectory().
#define ERROR_CREATE_TEMP_DIR لم يتمكّن مزيل التّثبيت من إنشاء الدليل المؤقّت: %s
#define ERROR_SETUP_REQUIREMENT مطلوب ويندوز 95 أو أعلى!  جاري الخروج من مزيل التّثبيت...
#define MB_WARNING_STR تحذير
#define ERROR_UNINSTALL_LOG_FOLDER لم يوجد مجلّد سجل مزيل التّثبيت:%s
#define MB_MESSAGE_STR رسالة
#define DLG_REMOVE_FILE_TITLE إزالة الملفّ
#define ERROR_GETVERSION فشل GetVersionEx()!
#define MB_ATTENTION_STR تنبيه
#define MSG_FORCE_QUIT_PROCESS وجد برنامج إزالة التّثبيت أنّ %s (%s) لا يزال يعمل.  إنقر موافق للخروج من %s و متابعة إزالة التّثبيت. يمكنك أيضا استخدام مدير مهام ويندوز للخروج من %s، ثمّ إنقر موافق لمتابعة إزالة التّثبيت.
#define MSG_FORCE_QUIT_PROCESS_FAILED سيخرج برنامج إزالة التّثبيت الآن.  لم يتمكن برنامج إزالة التّثبيت من المتابعة لأنّ %s (%s) لايزال يعمل. حاول الخروج من %s يدويا باستخدام مدير مهام ويندوز، ثمّ إبدأ برنامج إزالة التّثبيت مرّة ثانية.
#define MSG_DELETE_INSTALLATION_PATH لم تزل كلّ الملفات من دليل التّثبيت:\n\n  %s\n\nهل تريد حذف هذا الدّليل بشكل كامل?
#define MSG_INSTALLATION_PATH_WITHIN_WINDIR وجد برنامج التّثبيت أنّ مسار تثبيت $ProductNameInternal$ هو ضمن مجلّد ويندوز.  لن يحاول برنامج إزالة التّثبيت حذف التّثبيت بسبب إمكانية حذف ملفات ضرورية للنّظام.

#unfilter emptyLines
