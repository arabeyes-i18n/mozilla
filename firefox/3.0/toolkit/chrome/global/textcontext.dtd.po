# translation of textcontext.dtd.po to Arabic
# Khaled Hosny <khaledhosny@eglug.org>, 2007, 2008.
# extracted from en/toolkit/chrome/global/textcontext.dtd, ar/toolkit/chrome/global/textcontext.dtd
msgid ""
msgstr ""
"Project-Id-Version: textcontext.dtd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-02-01 00:36+0100\n"
"PO-Revision-Date: 2008-02-01 19:06+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: n==1 ? 0 : n==2 ? 1 : n>=3 && n<=10 ? 2 : 3\n"
"X-Generator: KBabel 1.11.4\n"

#: cutCmd.label cutCmd.accesskey
msgid "Cu&t"
msgstr "&قص"

#: copyCmd.label copyCmd.accesskey
msgid "&Copy"
msgstr "ا&نسخ"

#: pasteCmd.label pasteCmd.accesskey
msgid "&Paste"
msgstr "ا&لصق"

#: undoCmd.label undoCmd.accesskey
msgid "&Undo"
msgstr "ت&راجع"

#: selectAllCmd.label selectAllCmd.accesskey
msgid "Select &All"
msgstr "اختر ال&كل"

#: deleteCmd.label deleteCmd.accesskey
msgid "&Delete"
msgstr "اح&ذف"

#: spellAddToDictionary.label spellAddToDictionary.accesskey
msgid "Add t&o Dictionary"
msgstr "ضِف &في القاموس"

#: spellCheckEnable.label spellCheckEnable.accesskey
msgid "Check &Spelling"
msgstr "دقق الإ&ملاء"

#: spellNoSuggestions.label
msgid "(No Spelling Suggestions)"
msgstr "(لا اقتراحات إملاء)"

#: spellDictionaries.label spellDictionaries.accesskey
msgid "&Languages"
msgstr "الل&غات"

