# translation of downloadProgress.properties.po to Arabic
# Khaled Hosny <khaledhosny@eglug.org>, 2008.
# extracted from mozilla/mozilla/toolkit/chrome/global/downloadProgress.properties
msgid ""
msgstr ""
"Project-Id-Version: downloadProgress.properties\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-09-26 15:48+0300\n"
"PO-Revision-Date: 2008-01-30 17:20+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Accelerator-Marker: &\n"

# LOCALIZATION NOTE (BadPluginTitle):
#
#    This dialog is displayed when plugin throws unhandled exception
#
#: BadPluginTitle
msgid "Illegal Operation in Plug-in"
msgstr "عمليّة غير مشروعة في الملحق"

# LOCALIZATION NOTE (BadPluginMessage):
#
#    This is the message for the BadPlugin dialog.
#    %S will be replaced by brandShortName.
#
#: BadPluginMessage
msgid ""
"The plug-in performed an illegal operation. You are strongly advised to "
"restart %S."
msgstr "قام الملحق بعمليّة غير مشروعة. ينصح بشدّة بإعادة بدء %S."

# LOCALIZATION NOTE (BadPluginCheckboxMessage):
#
#    This message tells the user that if they check this checkbox, they
#    will never see this dialog again.
#
#: BadPluginCheckboxMessage
msgid "Don't show this message again during this session."
msgstr "لا تعرض هذه الرّسالة مجدّدًا خلال هذه الجّلسة."

