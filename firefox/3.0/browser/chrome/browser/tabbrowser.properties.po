# translation of tabbrowser.properties.po to Arabic
# Khaled Hosny <khaledhosny@eglug.org>, 2008.
# extracted from mozilla/mozilla/browser/chrome/browser/tabbrowser.properties
msgid ""
msgstr ""
"Project-Id-Version: tabbrowser.properties\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-02-01 00:36+0100\n"
"PO-Revision-Date: 2008-02-01 15:19+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: n==1 ? 0 : n==2 ? 1 : n>=3 && n<=10 ? 2 : 3\n"
"X-Generator: KBabel 1.11.4\n"
"X-Accelerator-Marker: &\n"

#: tabs.loading
msgid "Loading…"
msgstr "يُحمّل..."

#: tabs.untitled
msgid "(Untitled)"
msgstr "(غير مُعنون)"

#: tabs.closeTab
msgid "Close Tab"
msgstr "أغلق اللّسان"

#: tabs.close
msgid "Close"
msgstr "أغلق"

#: tabs.closeWarningTitle
msgid "Confirm close"
msgstr "أكّد الإغلاق"

#: tabs.closeWarningOneTab
msgid "You are about to close %S tab. Are you sure you want to continue?"
msgstr "أنت على وشك إغلاق %S لسان. أمتأكّد أنّك تريد المواصلة؟"

#: tabs.closeWarningMultipleTabs
msgid "You are about to close %S tabs. Are you sure you want to continue?"
msgstr "أنت على وشك إغلاق %S ألسنة. أمتأكّد أنّك تريد المواصلة؟"

#: tabs.closeButtonOne
msgid "Close tab"
msgstr "أغلق اللّسان"

#: tabs.closeButtonMultiple
msgid "Close tabs"
msgstr "أغلق الألسنة"

#: tabs.closeWarningPromptMe
msgid "Warn me when I attempt to close multiple tabs"
msgstr "حذِّر عند محاولة إغلاق عدّة ألسنة"

