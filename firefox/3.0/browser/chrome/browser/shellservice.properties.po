# translation of shellservice.properties.po to Arabic
# Khaled Hosny <khaledhosny@eglug.org>, 2008.
# extracted from mozilla/mozilla/browser/chrome/browser/shellservice.properties
msgid ""
msgstr ""
"Project-Id-Version: shellservice.properties\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-02-01 00:36+0100\n"
"PO-Revision-Date: 2008-02-01 15:18+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: n==1 ? 0 : n==2 ? 1 : n>=3 && n<=10 ? 2 : 3\n"
"X-Generator: KBabel 1.11.4\n"
"X-Accelerator-Marker: &\n"

#: optionsLabel
msgid "%S &Options"
msgstr "&خيارات %S"

#: safeModeLabel
msgid "%S &Safe Mode"
msgstr "وضع  %S الآ&من"

#: setDefaultBrowserTitle
msgid "Default Browser"
msgstr "المتصفّح المبدئي"

#: setDefaultBrowserMessage
msgid ""
"%S is not currently set as your default browser. Would you like to make it "
"your default browser?"
msgstr "‏%S ليس مضبوطا كمتصفّحك المبدئيّ. هل تريد جعله متصفّحك المبدئيّ؟"

#: setDefaultBrowserDontAsk
msgid "Always perform this check when starting %S."
msgstr "قم دائما بهذا الاختبار عند بدء %S."

#: alreadyDefaultBrowser
msgid "%S is already set as your default browser."
msgstr "‏%S مضبوط مسبقا كمتصفّحك المبدئي."

#: desktopBackgroundLeafNameWin
msgid "Desktop Background.bmp"
msgstr "Desktop Background.bmp"

#: DesktopBackgroundDownloading
msgid "Saving Picture…"
msgstr "يحفظ الصورة..."

#: DesktopBackgroundSet
msgid "Set Desktop Background"
msgstr "اجعلها خلفيّة سطح المكتب"

