# translation of migration.dtd.po to Arabic
# Abd Al-Rahman Hosny <abdohosny@gmail.com>, 2007.
# Khaled Hosny <khaledhosny@eglug.org>, 2007, 2008.
# extracted from en/browser/chrome/browser/migration/migration.dtd, ar/browser/chrome/browser/migration/migration.dtd
msgid ""
msgstr ""
"Project-Id-Version: migration.dtd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-02-01 00:36+0100\n"
"PO-Revision-Date: 2008-02-01 05:04+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Generator: KBabel 1.11.4\n"

#: migrationWizard.title
msgid "Import Wizard"
msgstr "مرشِد الاستيراد"

#: importFrom.label
msgid "Import Options, Bookmarks, History, Passwords and other data from:"
msgstr "استورد الخيارات، العلامات، التّاريخ، كلمات السرّ و بيانات أخرى من:"

#: importFromUnix.label
msgid "Import Preferences, Bookmarks, History, Passwords and other data from:"
msgstr "استورد التّفضيلات، العلامات، التّاريخ، كلمات السرّ و بيانات أخرى من:"

#: importFromBookmarks.label
msgid "Import Bookmarks from:"
msgstr "استورد العلامات من:"

#: importFromIE.label importFromIE.accesskey
msgid "&Microsoft Internet Explorer"
msgstr "&ميكروسوفت إنترنت إكسبلورر"

#: importFromPhoenix.label importFromPhoenix.accesskey
msgid "Firefox 0.8, Firebird or &Phoenix"
msgstr "&فيَرفُكس 0.8، فيربرد أو فيونكس"

#: importFromNothing.label importFromNothing.accesskey
msgid "&Don't import anything"
msgstr "&لا تستورد أي شيء"

#: importFromSeamonkey.label importFromSeamonkey.accesskey
msgid "&Netscape 6, 7 or Mozilla 1.x"
msgstr "&نتسكيب 6,7 أو موزيلا 1.*"

#: importFromNetscape4.label importFromNetscape4.accesskey
msgid "Netscape &4.x"
msgstr "نتسكيب &4.*"

#: importFromOpera.label importFromOpera.accesskey
msgid "&Opera"
msgstr "أوب&را"

#: importFromCamino.label importFromCamino.accesskey
msgid "&Camino"
msgstr "كامين&و"

#: importFromSafari.label importFromSafari.accesskey
msgid "&Safari"
msgstr "&سافاري"

#: importFromOmniWeb.label importFromOmniWeb.accesskey
msgid "Omni&Web"
msgstr "أُمن&ي وِب"

#: importFromICab.label importFromICab.accesskey
msgid "&iCab"
msgstr "&آيكاب"

#: importFromKonqueror.label importFromKonqueror.accesskey
msgid "&Konqueror"
msgstr "&كُنكَرر"

#: importFromEpiphany.label importFromEpiphany.accesskey
msgid "&Epiphany"
msgstr "&إبِفني"

#: importFromGaleon.label importFromGaleon.accesskey
msgid "&Galeon"
msgstr "&جاليون"

#: importFromFile.label importFromFile.accesskey
msgid "&From File"
msgstr "&من ملف"

#: noMigrationSources.label
msgid "No programs that contain bookmarks, history or password data could be found."
msgstr "لم يُعثر على أي برامج تحتوي على بيانات علامات، تاريخ، أو كلمات سرّ."

#: importSource.title
msgid "Import Settings and Data"
msgstr "استورد الإعدادات و البيانات"

#: importItems.title
msgid "Items to Import"
msgstr "العناصر المراد استيرادها"

#: importItems.label
msgid "Select which items to import:"
msgstr "اختر أي العناصر تريد استيرادها:"

#: migrating.title
msgid "Importing…"
msgstr "يستورد..."

#: migrating.label
msgid "The following items are currently being imported…"
msgstr "يجري استيراد العناصر التالية..."

#: selectProfile.title
msgid "Select Profile"
msgstr "اختر الملفّ الشّخصيّ"

#: selectProfile.label
msgid "The following profiles are available to import from:"
msgstr "تتوفّر الملفّات الشّخصيّة التّالية للاستيراد منها:"

#: done.title
msgid "Import Complete"
msgstr "تمّ الاستيراد"

#: done.label
msgid "The following items were successfully imported:"
msgstr "تمّ استيراد العناصر التّالية بنجاح:"

