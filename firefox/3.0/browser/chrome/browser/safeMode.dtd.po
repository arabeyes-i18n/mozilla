# translation of safeMode.dtd.po to Arabic
# Abd Al-Rahman Hosny <abdohosny@gmail.com>, 2007.
# Khaled Hosny <khaledhosny@eglug.org>, 2007, 2008.
# extracted from en/browser/chrome/browser/safeMode.dtd, ar/browser/chrome/browser/safeMode.dtd
msgid ""
msgstr ""
"Project-Id-Version: safeMode.dtd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-09-26 15:48+0300\n"
"PO-Revision-Date: 2008-01-30 14:47+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Generator: KBabel 1.11.4\n"

# ***** BEGIN LICENSE BLOCK *****
# #if 0
# - Version: MPL 1.1/GPL 2.0/LGPL 2.1
# -
# - The contents of this file are subject to the Mozilla Public License Version
# - 1.1 (the "License"); you may not use this file except in compliance with
# - the License. You may obtain a copy of the License at
# - http://www.mozilla.org/MPL/
# -
# - Software distributed under the License is distributed on an "AS IS" basis,
# - WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
# - for the specific language governing rights and limitations under the
# - License.
# -
# - The Original Code is mozilla.org Code.
# -
# - The Initial Developer of the Original Code is Mike Connor.
# - Portions created by the Initial Developer are Copyright (C) 2005
# - the Initial Developer. All Rights Reserved.
# -
# - Contributor(s):
# -   Mike Connor <mconnor@steelgryphon.com>
# -
# - Alternatively, the contents of this file may be used under the terms of
# - either the GNU General Public License Version 2 or later (the "GPL"), or
# - the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
# - in which case the provisions of the GPL or the LGPL are applicable instead
# - of those above. If you wish to allow use of your version of this file only
# - under the terms of either the GPL or the LGPL, and not to allow others to
# - use your version of this file under the terms of the MPL, indicate your
# - decision by deleting the provisions above and replace them with the notice
# - and other provisions required by the LGPL or the GPL. If you do not delete
# - the provisions above, a recipient may use your version of this file under
# - the terms of any one of the MPL, the GPL or the LGPL.
# -
# #endif
# - ***** END LICENSE BLOCK *****
#: safeModeDialog.title
msgid "&brandShortName; Safe Mode"
msgstr "وضع &brandShortName; الآمن"

#: window.width
msgid ""
"_: Do not translate this.  Only change the numeric values if you need this "
"dialogue box to appear bigger.\n"
"37em"
msgstr "37em"

#: safeModeDescription.label
msgid ""
"&brandShortName; is now running in Safe Mode, which temporarily disables "
"your custom settings, themes, and extensions."
msgstr "يشتغل &brandShortName; الآن في الوضع الآمن، وهذا يعطل الإعدادات الخاصة، والسِمات والامتدادات مؤقتا."

#: safeModeDescription2.label
msgid "You can make some or all of these changes permanent:"
msgstr "يمكنك تثبيت بعض أو كل هذه التغييرات:"

#: disableAddons.label disableAddons.accesskey
msgid "&Disable all add-ons"
msgstr "ع&طّل كل الإضافات"

#: resetToolbars.label resetToolbars.accesskey
msgid "&Reset toolbars and controls"
msgstr "صفّر الأ&شرطة و المفاتيح"

#: resetBookmarks.label resetBookmarks.accesskey
msgid "Reset &bookmarks to &brandShortName; defaults"
msgstr "صفّر &العلامات لمبدئيات &brandShortName;"

#: resetUserPrefs.label resetUserPrefs.accesskey
msgid "Reset all user &preferences to &brandShortName; defaults"
msgstr "ص&فّر كل خيارات المستخدم لمبدئيات &brandShortName;"

#: restoreSearch.label restoreSearch.accesskey
msgid "Re&store default search engines"
msgstr "استع&د محركات البحث المبدئية"

#: changeAndRestartButton.label
msgid "Make Changes and Restart"
msgstr "قم بالتغييرات و أعِد التشغيل"

#: continueButton.label
msgid "Continue in Safe Mode"
msgstr "واصِل في الوضع الآمن"

