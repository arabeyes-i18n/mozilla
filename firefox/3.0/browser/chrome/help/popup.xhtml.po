msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-02-01 00:36+0100\n"
"PO-Revision-Date: 2007-09-26 15:48+0300\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Generator: Translate Toolkit 1.0.1\n"

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:1
#: mozilla/mozilla/browser/chrome/help/popup.xhtml:2
msgid "Controlling Pop-ups"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:3
msgid ""
"This document explains all of the &pref.plural; available in "
"&brandFullName;   for controlling pop-ups."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:4
msgid "In this section:   <ul>"
msgstr "في هذا الفصل:   <ul>"

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:5
#: mozilla/mozilla/browser/chrome/help/popup.xhtml:7
msgid "What are Pop-ups?"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:6
#: mozilla/mozilla/browser/chrome/help/popup.xhtml:17
msgid "Pop-up Blocker &pref.pluralCaps;"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:8
msgid ""
"Pop-up windows, or pop-ups, are windows that appear automatically without "
"your permission. They vary in size but usually don't cover the whole screen. "
"Some pop-ups open on top of the current &brandShortName; window, while "
"others appear underneath &brandShortName; (pop-unders)."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:9
msgid ""
"&brandShortName; allows you to control both pop-ups and popunders through "
"the <a href=\"prefs.xhtml#content_options\">Content panel</a> in &pref."
"pluralCaps;. Pop-up blocking is turned on by default, so you don't have to "
"worry about enabling it to prevent pop-ups from appearing in "
"&brandShortName;."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:10
msgid ""
"When blocking a pop-up, &brandShortName; displays an information bar, as "
"well as an icon <img src=\"chrome://browser/skin/Info.png\" width=\"16\" "
"height=\"16\" alt=\"\"/> in the status bar. When you click either the "
"<em>&pref.pluralCaps;</em> button in the information bar or the icon in the "
"status bar, a menu is displayed with the following choices:"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:11
msgid "Allow/Block pop-ups for this site"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:12
msgid ""
"Edit <a href=\"#popup_blocker_preferences\">Pop-up Blocker     &pref."
"pluralCaps;…</a>"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:13
msgid "Don't show this message (info message) when pop-ups are blocked"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:14
msgid "(show a blocked pop-up)"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:15
msgid ""
"<strong>Blocking pop-ups may interfere with some web sites</strong>: Some "
"web sites, including some banking sites, use pop-ups for important features. "
"Blocking all pop-ups disables such features. To allow specific web sites to "
"use pop-ups, while blocking all others, you can add specific web sites to "
"the list of allowed sites."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:16
msgid ""
"<strong>Blocking pop-ups doesn't always work</strong>: Although "
"&brandShortName; blocks most pop-ups, some web sites may show pop-ups using "
"uncovered methods, even when blocked."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:18
msgid ""
"The Pop-up Blocker &pref.pluralCaps; are located in the <a   href=\"prefs."
"xhtml#content_options\">Content panel</a> of &pref.menuPath;."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:19
msgid "From there, you can do the following things:"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:20
msgid ""
"<strong>Block pop-up windows</strong>: Deselect this &pref.singular; to     "
"disable the pop-up blocker altogether."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:21
msgid "Exceptions dialog"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:22
msgid ""
"<strong>Allow</strong>: Click this to add a web site to the exceptions list."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:23
msgid ""
"<strong>Remove Site</strong>: Click this to remove a web site from the "
"exceptions list."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:24
msgid ""
"<strong>Remove All Sites</strong>: Click this to remove all of the web sites "
"in       the exceptions list."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:25
msgid ""
"<strong>Note</strong>: Blocking pop-ups may not always work and may "
"interfere with some web sites. For more information about blocking pop-ups, "
"see <a href=\"#what_are_popups\">What are Pop-ups</a>."
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:26
msgid "12 September 2005"
msgstr ""

#: mozilla/mozilla/browser/chrome/help/popup.xhtml:27
msgid ""
"Copyright &copy; &copyright.years; Contributors to the Mozilla Help Viewer "
"Project."
msgstr ""
